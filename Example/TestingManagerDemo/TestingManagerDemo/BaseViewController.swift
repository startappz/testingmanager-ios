//
//  BaseController.swift
//  TestingManagerDemo
//
//  Created by Rawan on 11/07/2021.
//

import UIKit

class BaseViewController: UIViewController {
    
    override var canBecomeFirstResponder: Bool {
        true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        becomeFirstResponder()
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            app.testingManager.present()
        }
    }
}
