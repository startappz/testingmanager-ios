//
//  TestingManagerConfigurator.swift
//  TestingManagerDemo
//
//  Created by Rawan on 06/07/2021.
//

import UIKit
import TestingManager_SDK

enum testingManagrMocks: String, Mock {
    
    case APIVersion
    case userName
    case salary
    case button
    case toggle
    case environmentOverride
    
    var title: String {
        switch self {
        case .APIVersion:
            return "API Version"
        case .userName:
            return " User Name"
        case .salary:
            return "Salary"
        case .button:
            return "Button"
        case .toggle:
            return "Toggle"
        case .environmentOverride:
            return "Environment override"
        }
    }
    
    var tag: String {
        self.rawValue
    }
    
    var type: MockType {
        
        switch self {
        case .APIVersion:
            return .value(.asciiCapableNumberPad)
        case .userName:
            return .value(.default)
        case .salary:
            return .list(["500","1000","2000"], isMultipleSelection: true)
        case .button:
            return .button(handler: {
                print("Button tap")
            })
        case .toggle:
            return .toggle
        case .environmentOverride:
            return .list(["DEV", "UAT", "PROD"], isMultipleSelection: false)
        }
    }
}
