//
//  ViewController.swift
//  TestingManagerDemo
//
//  Created by Rawan on 06/07/2021.
//

import UIKit

final class ViewController: BaseViewController, UITextFieldDelegate {
 
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var APIUVersionLabel: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var environmentOverrideLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textfield.delegate = self
        app.testingManager.onSave = {
            self.getDataFromTestingManager()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textfield.resignFirstResponder()
        return true
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        getDataFromTestingManager()
    }
    
    private func getDataFromTestingManager() {
        
        // get user name from testing manager
        if let mockUserName = app.testingManager.value(forMock: testingManagrMocks.userName) as? String {
            userNameLabel.text = "Mocked user name: \(mockUserName)"
        }
        else {
            userNameLabel.text = "User Name"
        }
    
        // get API version from testing manager
        if let mockedAPIVersion = app.testingManager.value(forMock: testingManagrMocks.APIVersion) as? String {
            APIUVersionLabel.text = "Mocked API version: \(mockedAPIVersion)"
        }
        else {
            APIUVersionLabel.text = "API Version"
        }
        
        // Environment Override
        if app.testingManager.isMockEnabeld(testingManagrMocks.environmentOverride),
           let envOverride = app.testingManager.value(forMock: testingManagrMocks.environmentOverride) as? [String] {
            setEnvironmentOverrideLabel(envOverride.first)
        } else {
            setEnvironmentOverrideLabel(nil)
        }
    }
    
    private func setEnvironmentOverrideLabel(_ override: String?) {
        
        let title = "Environment Override: "
        environmentOverrideLabel.text = title + (override ?? "null")
    }
}

