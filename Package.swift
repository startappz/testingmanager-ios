// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TestingManager_SDK",
    platforms: [
        .iOS(.v11)
    ],
     products: [
        .library(
            name: "TestingManager_SDK",
            targets: ["TestingManager_SDK"]),
    ],
    targets: [
        .target(name: "TestingManager_SDK",
                path: "Sources/TestingManager_SDK")
    ]
)
