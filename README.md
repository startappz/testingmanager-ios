# Testing Manager

Testing manager is a tool thats allow developer/tester to mock any value he wants.

## Requirements
- XCode 13
- Swift 5

## Installation

### Swift Package Manager

Testing manager is available through SPM: `https://bitbucket.org/startappz/testingmanager-ios.git`

### Manually

1. Add SDK source to your project.
2. Link SDK with your target.


## Usage

### Configuration

To configure testing manager you should call `configure(testingManagerEnabled: Bool, mocks: [Mock])`, it accepts 2 parameters:
- `testingManagerEnabled`: pass true/false depends on your build type, usually you want testing manager to be enabled for staging builds and disabled for uat and production builds. 
- `mocks`: array of testing manager line items (mocks), each array item should conform to `Mock` protocol, you can find below example enumeration of mocks.

Make sure you call `configure` function when app window is already created, testing manager needs app window to add shake gesture handler to it.

```swift
import TestingManager_SDK

enum TestingManagerDatasource: String, Mock, CaseIterable {
    
    case mockVersion
    case logout
    case skipAPI
    case salary
    
    var tag: String {
        self.rawValue
    }
    
    var title: String {
        switch self {
        case .mockVersion:
            return "Mock-Version"
        case .logout:
            return "Logout"
        case .skipAPI:
            return "Mock API"
        case .salary:
            return "Salary"
        }
    }
    
    var type: MockType {
        switch self {
        case .mockVersion:
            return .value
        case .logout:
            return .button(handler: {
                // logout
            })
        case .skipAPI:
            return .toggle
        case .salary:
            return .list(["500","1000","2000"])
        }
    }
}
```

```swift
import TestingManager_SDK

let testingManager = TestingManager()
testingManager.configure(
    testingManagerEnabled: true, // In real apps use compiler flags like DEBUG or STAGING to control testing manager presence
    mocks: TestingManagerDatasource.allCases)
```

### Presentation
Testing manager can be presented using `present()` function, it will show testing manager if it's enabled.
To hide testing manager just tap on gray background around testing manager dialog or you will see close button at the bottom of screen in case testing manager dialog reached screen edges and it's not a lot of space around it.

Usually you want to present testing manager by device shake, to do that you need to override `motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?)` in any `UIResponder` (`UIApplication`, `UIWindow`, `UIView`, `UIViewController`) subclass and keep in mind that this subclass should always stay active. In case of `UIApplication` and `UIWindow` it's simple because you usually have single entity of those, in case of `UIViewController` you should do override in base view controller class or view controller that's always on screen (for example subclass of  `UINavigationViewController` used as an entry navigation point in the app), in case of `UIView` you will also need to move focus back using `becomeFirstReponder` when focus moves to textfield,searchbar,etc. You can find below probably the easiest implementation using base view controller subclass:
```swift
class BaseViewController: UIViewController {
    
    override var canBecomeFirstResponder: Bool {
        true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        becomeFirstResponder()
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            app.testingManager.present()
        }
    }
}
```

### Mock values access
- To know is mock enabled or no, just call `testingManager.isMockEnabeld(TestingManagerDatasource.mockVersion)` it returns bool value.
- To know mock value, just call `testingManager.value(forMock: TestingManagerDatasource.mockVersion) as? String` it returns `String` (in this case).

### Additional handlers

- `onSave` handler: this handler is called after mock values are changed & saved, you can use it as below:
```swift
testingManager.onSave = { in
    // some work when testing manager values are changed & saved
}
```

## Mock types

You will get the value for your mock based on its type, also each mock will be shown on the box based on its type (text field, button, toggle or list ). You can find below description for all available types:

1. `value` which will show text filed to enter your mocked value on it, will returns a String
2. `list` multi or single selection list, will return an array of strings
3. `button` to perform a selector once the button tapped
4. `toggle` to mock a bool value, so its return boolean

## How to use Testing manager for backend environment on-the-fly switch in your app
### Why
Usually we have different environments for backend like development, uat, production, etc. The testers can't switch backend environment on the fly, but it would be great to have it in order to reduce amount of builds or just to be more flexible during the testing process. For example development build was created with ability to switch backend environment on the fly. Testers done with development build testing and proceed to staging build testing just using testing manager to switch backend environment on the fly.
### How
You'll need Testing manager SDK version 1.3.0 or above. Let's assume we have 3 backend environments: dev, uat and prod described in `AppInfo` class and it can return current environment based on your internal rules (e.x. swift compilation flag).

```swift
enum AppInfo {
    
    enum Environment: String {
        case dev
        case uat
        case prod
    }
    
    static var environment: Environment {
        
        if isDev {
            return .dev
        } else if isUAT {
            return .uat
        } else if isProd {
            return .prod
        } else {
            return .prod
        }
    }

    private static var isDev: Bool {
        #if DEV // Swift compilation flag
        return true
        #else
        return false
        #endif
    }
}
```

The only difference between the environments is base URL and we have a property for base URL depending on build environment:
```swift
var backendBaseURL: String {
    switch AppInfo.environment {
    case .dev:
        return "https://dev.com"
    case .uat:
        return "https://uat.com"
    case .prod:
        return "https://prod.com"
    }
}
```

To add backend environment on-the-fly switch to testing manager we add new testing manager item of List type passing environment names we want to override. We'll do it in `TestingManagerUtility` class which encapsulate Testing manager SDK in our project.
```swift
class TestingManagerUtility {
    
    enum Datasource: String, Mock, CaseIterable {
        
        case appEnvironmentOverride
        
        var title: String {
            switch self {
            case .appEnvironmentOverride:
                return "Environment Override"
            }
        }

        var type: MockType {
            switch self {
            case .appEnvironmentOverride:
                return .list([
                    AppInfo.Environment.dev.rawValue,
                    AppInfo.Environment.uat.rawValue,
                    AppInfo.Environment.prod.rawValue
                ], isMultipleSelection: false)
            }
        }
    }
}
```

Then let's create `appEnvironmentOverride` property in `TestingManagerUtility`, the property will return selected environment or nil in case it's not selected.
```swift
class TestingManagerUtility {

    var appEnvironmentOverride: AppInfo.Environment? {
        guard AppInfo.isTestingManagerEnabled else {
            return nil
        }
        if
            testingManager.isMockEnabeld(Datasource.appEnvironmentOverride),
            let overrideEnvironmentId = (testingManager.value(forMock: Datasource.appEnvironmentOverride) as? [String])?.first,
            let environment = AppInfo.Environment(rawValue: overrideEnvironmentId)
        {
            return environment
        }
        return nil
    }
}
```
Also in the code snippet above we check if testing manager is enabled for build type. We need this check in order to avoid issues when testers install build that doesn't support testing manager over the build that supports testing manager, since environment overriding info is keeping in NSUserDefaults similar to other testing manager items. In our case testing manager is enabled only for dev builds.
```swift
enum AppInfo {
    static var isTestingManagerEnabled: Bool {
        isDev
    }
}
```

Finally we can use `appEnvironmentOverride` from `TestingManagerUtility` for `backendBaseURL` property. We check first if there's override environment and switch based on it, if no fallback to build type environment (based on swift compilation flag). Remember  `appEnvironmentOverride` will be nil in case environment override is not selected or testing manager is disabled. You can check environment override example (UI part) in the testing manager demo project.
```swift
var backendBaseURL: String {
    switch app.testingManager.appEnvironmentOverride ?? AppInfo.environment {
    case .dev:
        return "https://dev.com"
    case .uat:
        return "https://uat.com"
    case .prod:
        return "https://prod.com"
    }
}
```