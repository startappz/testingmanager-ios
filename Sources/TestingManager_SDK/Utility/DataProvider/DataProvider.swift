//
//  CachUtility.swift
//  TestingManager
//
//  Created by Rawan on 07/07/2021.
//

import UIKit

final class DataProvider {

    static let shared = DataProvider()

    private let keyPrefix = "startappz.com.TestingManager_KEY"
    private let isEnabledKey = "startappz.com.TestingManager_KEY_IS_ENABLED"
    
    var mocks:[Mock] = [Mock]()
    
    func setup(mocks:[Mock]) {
        
        self.mocks = mocks
        clearRemovedMocks() // clear any removed item from User defuals
    }
    
    
    func setTestingManager(enabled: Bool) {
        
        UserDefaults.standard.set(enabled, forKey: isEnabledKey)
        if !enabled {
            clearAll()
        }
    }
    
    func isTestingManagerEnabled() -> Bool {
    
        UserDefaults.standard.value(forKey: isEnabledKey) as? Bool ?? false
    }
    
    // clear values for removed mocks
    func clearRemovedMocks() {
        
        guard let oldSavedKeys = allSavedeKeys() else {
            return
        }
        
        var allKeys = [String]()
        for mock in mocks {
            allKeys.append(key(forMock: mock))
        }
        
        let removedKeys = oldSavedKeys.filter{
            let oldSavedKey = $0
            return !allKeys.contains{ oldSavedKey == $0 }
        }
        
        //clear removed keys
        for removedKey in removedKeys {
            UserDefaults.standard.removeObject(forKey: removedKey)
        }
    }
    
    // clear value for disabled mocks
    func clearDisableMocks(_ enabledMocksKeys: [String]) {
        
        guard let oldSavedKeys = allSavedeKeys() else {
            return
        }
  
        let removedKeys = oldSavedKeys.filter{
            let oldSavedKey = $0
            return !enabledMocksKeys.contains{ oldSavedKey == $0 }
        }
        
        //clear removed keys
        for removedKey in removedKeys {
            UserDefaults.standard.removeObject(forKey: removedKey)
        }
    }

    // save mock value after hide testing manager view
    func save(mocks: [(mock: Mock, value: Any?)]) {
        
      var mockKeys = [String]()
        for (mock, value) in mocks {
            UserDefaults.standard.setValue(value, forKey: key(forMock: mock))
            mockKeys.append(key(forMock: mock))
        }

        // disable mocks
        clearDisableMocks(mockKeys)
        UserDefaults.standard.setValue(mockKeys,forKey: "\(keyPrefix)_ALL_KEYS")
    }
        
    // check if mock is enabled
    func isEnabled(mock: Mock) -> Bool {
            
        guard isTestingManagerEnabled() else { return false }
        return UserDefaults.standard.value(forKey: key(forMock: mock)) != nil
    }

    // get saved value for mock
    func value(forMock mock: Mock) -> Any? {
        
        guard isTestingManagerEnabled() else { return nil }
        return UserDefaults.standard.value(forKey: key(forMock: mock))
    }

}


private extension DataProvider {
    
    func key(forMock mock: Mock)-> String {
        
        return "\(keyPrefix)_\(mock.tag)"
    }
    
    func allSavedeKeys() -> [String]? {
        
        return UserDefaults.standard.array(forKey: "\(keyPrefix)_ALL_KEYS") as? [String]
    }
    
    func clearAll() {
        guard let savedKeys = allSavedeKeys() else {
            return
        }
        
        for key in savedKeys {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }

}
