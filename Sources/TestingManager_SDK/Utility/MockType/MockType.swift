//
//  File.swift
//  
//
//  Created by Rawan on 20/09/2021.
//

import Foundation
import UIKit

/**
  #Mock types
 1.`value` which will show text filed to enter your mocked value on it, will returns a String
 2. `list` is multi selection list, will returns an array of strings
 3. `button` to perform a selector once the button tapped
 4. `toggle` to mock a bool value, so its return boolean
**/

public enum MockType {
    case value(_ keyboard: UIKeyboardType = .asciiCapableNumberPad)
    case list(_ items:[String], isMultipleSelection: Bool)
    case button(handler: () -> Void)
    case toggle
}
