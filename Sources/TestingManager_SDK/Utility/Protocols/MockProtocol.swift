//
//  File.swift
//  
//
//  Created by Rawan on 20/09/2021.
//

import Foundation

///
/// Mock have unique `tag`,
/// the `title` which will be shown in testing manager box
/// and `type` for that mock, check `MockType` for more details
/// All mocks should be confirmed to Mock protocol
///

public protocol Mock {
    var tag: String { get }
    var title: String { get }
    var type: MockType { get }
}
