//
//  Model.swift
//  TestingManager
//
//  Created by Rawan on 06/07/2021.
//

import UIKit
import CoreMotion

public class TestingManager {
    
    public var onSave: (() -> Void)?
    
    var isEnabled: Bool {
        DataProvider.shared.isTestingManagerEnabled()
    }
    
    public required init() {
    }
    
    public func present() {
        
        if DataProvider.shared.isTestingManagerEnabled() {
            show()
        }
    }

    func show() {
                
        // don't present testing manager if it's already presented
        if UIApplication.getCurrentViewController() is MainScreenViewController || UIApplication.getCurrentViewController() is MockListPopup {
            print("[TESTING_MANAGER] Unable to present testing manager, it's already presented")
            return
        }
        
        let frameworkBundle = Bundle.module

        let storyboard = UIStoryboard(name: "TestingManagerMain", bundle: frameworkBundle)

        guard let main = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as? MainScreenViewController else {
            return
        }
        main.onSave = onSave
        main.modalPresentationStyle = .overCurrentContext
        main.modalTransitionStyle = .crossDissolve
        if let root = UIApplication.getCurrentViewController() {
            root.present(main, animated: true)
        }
    }
    
    public func configure(
        testingManagerEnabled: Bool,
        mocks:[Mock]) {
        
        DataProvider.shared.setTestingManager(enabled: testingManagerEnabled)
        DataProvider.shared.setup(mocks: mocks)
    }
    
    public func value(forMock mock: Mock) -> Any? {
    
        guard isEnabled else { return nil }
        return DataProvider.shared.value(forMock: mock)
    }
    
    public func isMockEnabeld(_ mock: Mock) -> Bool {
        
        guard isEnabled else { return false }
        return value(forMock: mock) != nil
    }
}
