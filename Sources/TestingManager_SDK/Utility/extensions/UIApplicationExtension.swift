//
//  UIApplicationExtention.swift
//  TestingManager
//
//  Created by Rawan on 08/07/2021.
//

import UIKit

extension UIApplication {
    /// Get current visible view  controller
    class func getCurrentViewController(
        base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController)
    -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getCurrentViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getCurrentViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getCurrentViewController(base: presented)
        }
        return base
    }
    
    class var currentWindow: UIWindow? {
        if #available(iOS 13.0, *) {
            if let w = UIApplication.shared.delegate?.window, let window = w {
                return window
            }
            let w = UIApplication
                .shared
                .connectedScenes
                .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                .first
            if let window = w {
                return window
            }
            return nil
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}
