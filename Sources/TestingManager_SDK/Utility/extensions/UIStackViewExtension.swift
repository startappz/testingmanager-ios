//
//  UIStackViewExtention.swift
//  TestingManager
//
//  Created by Rawan on 07/07/2021.
//

import UIKit

extension UIStackView {
    
    func customize(color: UIColor,
                   radiusSize: CGFloat = 0,
                   maskedCorners: CACornerMask = [.layerMinXMinYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner ]) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
        
        subView.layer.cornerRadius = radiusSize
        subView.layer.masksToBounds = true
        subView.clipsToBounds = true
        subView.layer.maskedCorners = maskedCorners
    }
    
    func addMargin(
        top: CGFloat,
        left: CGFloat,
        bottom: CGFloat,
        right: CGFloat) {
        
        layoutMargins = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        isLayoutMarginsRelativeArrangement = true
    }

}
