//
//  UIViewExtention.swift
//  TestingManager
//
//  Created by Rawan on 07/07/2021.
//

import UIKit

extension UIView {
    
    func loadFromNib() {
        let  frameworkBundle = Bundle.module
        guard let view = frameworkBundle.loadNibNamed(
                String(describing: type(of: self)),
                owner: self,
                options: nil)?.first as? UIView else {
            return
        }
        
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        view.constrainEdges(to: self)
    }
    
    func constrainEdges(to view: UIView) {
        
        leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }

}
