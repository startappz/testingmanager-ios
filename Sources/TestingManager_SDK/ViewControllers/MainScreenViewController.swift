//
//  ViewController.swift
//  TestingManager
//
//  Created by Rawan on 04/07/2021.
//

import UIKit

public final class MainScreenViewController: UIViewController {
    
    var onSave: (() -> Void)?
    
    //MARK:- private properties
    @IBOutlet private weak var mocksStackView: UIStackView!
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet weak var backgroundButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    private var viewLoadedFirstTime = true
    private var mockViews: [MockView] = [MockView]()
    
    //MARK:- view life cycle
    public override func viewDidLoad() {
        
        super.viewDidLoad()
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if viewLoadedFirstTime {
            DispatchQueue.main.async {
                self.setupMocksList()
            }
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        viewLoadedFirstTime = false
    }

    //MARK:- Helpers
    private func setupUI() {
        
        closeButton.isHidden = true
        closeButton.layer.cornerRadius = 6
        closeButton.clipsToBounds = true
        backgroundButton.backgroundColor = .black
        backgroundButton.alpha = 0.2
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
    }
    
    @IBAction func onBackgroundTap(_ sender: Any) {
        saveAndHide()
    }
    
    @IBAction func onClose(_ sender: UIButton) {
        saveAndHide()
    }
    
    private func setupMocksList() {
        
        let mocks = DataProvider.shared.mocks
        // add mocks views as its type
        for mock in mocks {
            switch mock.type {
            case .value:
                let mockView = MockValueView(mock: mock)
                mockViews.append(mockView)
                mocksStackView.addArrangedSubview(mockView)

            case let .list(items, isMultipleSelection):
                let mockView = MockListView(mock: mock, list: items, isMultipleSelection: isMultipleSelection)
                mockView.delegate = self
                mockViews.append(mockView)
                mocksStackView.addArrangedSubview(mockView)
                
            case let .button(handler):
                let mockButtonView = MockButtonItemView(
                    title: "    \(mock.title)    ",
                    handler: handler)
                mocksStackView.addArrangedSubview(mockButtonView)
                
            case .toggle:
                let toggleView = MockToggleItemView(mock: mock)
                mockViews.append(toggleView)
                mocksStackView.addArrangedSubview(toggleView)
            }
        }
        
        show()
    }
        
    private func show() {
        // close testing manager view
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            DispatchQueue.main.async {
                self.manageCloseButtonAvailability()
            }
        })
    }
    
    private func hide() {
        
        // close testing manager view
        mocksStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        dismiss(animated: true)
    }
    
    private func manageCloseButtonAvailability() {
        
        let contentHeight = contentView.frame.height
        
        let safeAreaTopPadding = UIApplication.currentWindow?.safeAreaInsets.top ?? 0
        let safeAreaBottomPadding = UIApplication.currentWindow?.safeAreaInsets.bottom ?? 0
        let windowHeight = UIApplication.currentWindow?.frame.height ?? 0
        let safeAreaHeight = safeAreaBottomPadding + safeAreaTopPadding
        let padding: CGFloat = 140
        
        closeButton.isHidden = contentHeight < windowHeight - safeAreaHeight - padding
    }
    
    private func saveMockValues() {
        
        var enabledMocks = [(Mock,Any?)]()
        // get enabled mocks to save its values
        for mockView in mockViews {
            if mockView.isEnabled {
                enabledMocks.append((mockView.mock,mockView.getValue()))
            }
        }
        DataProvider.shared.save(mocks: enabledMocks)
    }

    /// Save values then hide the view
    func saveAndHide() {
        
        saveMockValues()
        hide()
        onSave?()
    }
}

extension MainScreenViewController: MockListViewDelegate {
    
    func showList(_ items: [String], fromView view: MockListView, _ isMultipleSelection: Bool) {
        let listPopup = MockListPopup(
            list: items,
            selectedItems: DataProvider.shared.value(forMock: view.mock) as? [String] ?? [],
            isMultipleSelection: isMultipleSelection)
        listPopup.delegate = view
        listPopup.modalTransitionStyle = .crossDissolve
        present(listPopup, animated: true)
    }
}
