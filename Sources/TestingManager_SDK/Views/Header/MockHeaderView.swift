//
//  MockHeaderView.swift
//  TestingManager
//
//  Created by Rawan on 05/07/2021.
//

import UIKit

protocol MockHeaderViewDelegate: AnyObject {
    func didChangeStatus(_ enabled: Bool)
}

final class MockHeaderView: UIView {

    //MARK:- private properties
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var switchView: UISwitch!
    
    //MARK:- public properties
    weak var delegate: MockHeaderViewDelegate?
    
    //MARK:- view life cycle
    init() {
        
        super.init(frame: .zero)
        loadFromNib()
    }
    
    required init?(coder: NSCoder) {
        
        super.init(coder: coder)
        loadFromNib()
    }
    
    
    //MARK:- Utilities
    func set(title: String) {
        
        titleLabel.text = title
    }

    func setOn(_ isOn:Bool ) {
    
        switchView.isOn = isOn
    }
    
    //MARK:- Actions
    @IBAction func changeStatus(_ sender: UISwitch) {
        
        delegate?.didChangeStatus(sender.isOn)
    }
}
