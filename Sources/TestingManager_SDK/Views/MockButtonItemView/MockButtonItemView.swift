//
//  MockButtonItemView.swift
//  
//
//  Created by Roman Anistratenko on 12.09.2021.
//

import UIKit

class MockButtonItemView: UIView {
    
    @IBOutlet private weak var button: UIButton!
    
    private let title: String
    private let handler: () -> Void
    
    init(
        title: String,
        handler:@escaping () -> Void) {
        
        self.title = title
        self.handler = handler
        super.init(frame: .init(origin: .zero, size: .init(width: 100, height: 45)))
        loadFromNib()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("Not Supported Use init(mock: Mock)")
    }
    
    private func setupUI() {
        
        button.layer.cornerRadius = 6
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: #selector(onButtonTap), for: .touchUpInside)
    }
    
    @objc private func onButtonTap() {
        handler()
    }
}
