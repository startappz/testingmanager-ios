//
//  MockListView.swift
//  TestingManager
//
//  Created by Rawan on 06/07/2021.
//

import UIKit

class MockListItemView: UIView {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var radioButtonImage: UIImageView!
    
    var item: String
    var isSelected: Bool = false {
        didSet {
            updateUI()
        }
    }
    private let onSelect: (_ item: String) -> Void
    
    init(
        item: String,
        isSelected: Bool,
        onSelect: @escaping (_ item: String) -> Void
    ) {
        self.item = item
        self.isSelected = isSelected
        self.onSelect = onSelect
        super.init(frame: .zero)
        loadFromNib()
        setup()
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        
        titleLabel.text = item
        updateUI()
    }
    
    private func updateUI() {
        
        radioButtonImage.image = isSelected
            ? UIImage(named: "checkmark.circle", in: Bundle.module, compatibleWith: nil)
            : UIImage(named: "circle", in: Bundle.module, compatibleWith: nil)
    }
    
    @IBAction func selectListItem(_ sender: Any) {
        
        onSelect(item)
    }
}
