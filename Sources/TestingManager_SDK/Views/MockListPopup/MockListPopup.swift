//
//  MockListPopup.swift
//  TestingManager
//
//  Created by Rawan on 06/07/2021.
//

import UIKit
protocol MockListPopupDelegate {
    func MockListPopupDidSelectItems(_ value: [String])
}

class MockListPopup: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    private var itemViews = [MockListItemView]()
    
    var list: [String]
    var selectedItems: [String]
    var isMultipleSelection: Bool
    
    var delegate: MockListPopupDelegate?
    
    
    init(list: [String],
         selectedItems: [String],
         isMultipleSelection: Bool) {
        
        let frameworkBundle = Bundle.module
        self.selectedItems = selectedItems
        self.list = list
        self.isMultipleSelection = isMultipleSelection
        super.init(nibName: "MockListPopup", bundle: frameworkBundle)
    }
    
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setup()
    }

    func setup() {
        
        for item in list {
            let itemView = MockListItemView(item: item, isSelected: selectedItems.contains(item), onSelect: { selectedItem in
                if self.isMultipleSelection {
                    self.select(selectedItem)
                } else {
                    self.itemViews.filter({ $0.item != selectedItem }).forEach {
                        $0.isSelected = false
                    }
                    self.select(selectedItem)
                }
            })
            itemViews.append(itemView)
            stackView.addArrangedSubview(itemView)
        }
    }
    
    private func select(_ item: String) {
        
        let selectedItemView = self.itemViews.first { $0.item == item }
        selectedItemView?.isSelected.toggle()
    }
    
    @IBAction func close(_ sender: Any) {
        
        var selectedItems = [String]()
        for itemView in itemViews {
            if itemView.isSelected {
                selectedItems.append(itemView.item)
            }
        }
        
        delegate?.MockListPopupDidSelectItems(selectedItems)
        dismiss(animated: false)
    }
}
