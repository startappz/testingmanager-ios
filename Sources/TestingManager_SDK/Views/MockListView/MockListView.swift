//
//  MockListView.swift
//  TestingManager
//
//  Created by Rawan on 05/07/2021.
//

import UIKit

protocol MockListViewDelegate {
    func showList(_ items: [String], fromView view: MockListView, _ isMultipleSelection: Bool)
}

class MockListView: UIView, MockView {
    
    @IBOutlet weak var changeSelectedeItemsButton: UIButton!
    @IBOutlet weak var headerView: MockHeaderView! {
        didSet{
            headerView.delegate = self
        }
    }

    var delegate: MockListViewDelegate?
    
    var isEnabled: Bool = false
    var mock: Mock
    
    
    var items: [String]
    var selectedItems = [String]()
    var isMultipleSelection: Bool
    
    func getValue() -> Any? {
        selectedItems
    }
    
    init(mock: Mock, list: [String], isMultipleSelection: Bool) {
        
        self.mock = mock
        self.items = list
        self.isMultipleSelection = isMultipleSelection
        super.init(frame: .zero)
        loadFromNib()
        headerView.set(title: mock.title)
        selectedItems = DataProvider.shared.value(forMock: mock) as? [String] ?? []
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
        
    private func toggleStatus() {
        
        if isEnabled {
            changeSelectedeItemsButton.isHidden = false
            delegate?.showList(items, fromView: self, isMultipleSelection)
        }
        else {
            changeSelectedeItemsButton.isHidden = true
        }
    }
    
    private func setupUI() {
  
        if DataProvider.shared.isEnabled(mock: mock) {
            isEnabled = true
            headerView.setOn(true) // set switch on
            changeSelectedeItemsButton.isHidden = false
        }
        else {
            isEnabled = false
            headerView.setOn(false) // disable switch
            changeSelectedeItemsButton.isHidden = true
        }
    }

    @IBAction func changeSelectedeItems(_ sender: Any) {
        
        delegate?.showList(items, fromView: self, isMultipleSelection)
    }
}


extension MockListView: MockHeaderViewDelegate {
    
    func didChangeStatus(_ enabled: Bool) {
        
        isEnabled = enabled
        toggleStatus()
    }
    
}

extension MockListView: MockListPopupDelegate {
    
    func MockListPopupDidSelectItems(_ value: [String]) {
        
        selectedItems = value
        DataProvider.shared.save(mocks: [
            (mock, value)
        ])
    }

}
