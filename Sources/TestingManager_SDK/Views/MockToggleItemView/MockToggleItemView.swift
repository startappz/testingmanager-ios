//
//  MockToggleItemView.swift
//  
//
//  Created by Roman Anistratenko on 13.09.2021.
//

import UIKit

class MockToggleItemView: UIView, MockView {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var switchView: UISwitch!
    
    var isEnabled: Bool = false
    var mock: Mock
    
    init(mock: Mock) {
        
        self.mock = mock
        super.init(frame: .zero)
        loadFromNib()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        
        switchView.addTarget(self, action: #selector(changeSwitch), for: .valueChanged)
        titleLabel.text = mock.title
        
        if DataProvider.shared.isEnabled(mock: mock) {
            isEnabled = true
            switchView.setOn(true, animated: false)
        }
        else {
            isEnabled = false
            switchView.setOn(false, animated: false)
        }
    }
    
    func getValue() -> Any? {
        isEnabled ? true : nil
    }
    
    @objc private func changeSwitch() {
        
        isEnabled = switchView.isOn
    }
}
