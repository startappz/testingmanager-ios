//
//  TextFiled.swift
//  TestingManager
//
//  Created by Rawan on 04/07/2021.
//

import UIKit

protocol MockView {
    var isEnabled: Bool { get }
    var mock: Mock { get }
    func getValue()-> Any?
}

class MockValueView: UIView, MockView {
    
    //MARK:- private properties
    @IBOutlet private weak var actionView: UIView!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var headerView: MockHeaderView! {
        didSet{
            headerView.delegate = self
        }
    }
    
    //MARK:- public properties
    var isEnabled: Bool = false
    var mock: Mock
    
    //MARK:- view life cycle
    init(mock: Mock) {
        
        self.mock = mock
        super.init(frame: .zero)
        loadFromNib()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("Not Supported Use init(mock: Mock)")
    }
    
    //MARK:- Utilities
    private func setupUI() {
        
        textField.delegate = self
        if case let .value(keyboard) = mock.type {
            textField.keyboardType = keyboard
            if keyboard == .asciiCapableNumberPad || keyboard == .numberPad || keyboard == .decimalPad || keyboard == .phonePad {
                addDoneButtonToTextfieldKeeb()
            }
        }
        headerView.set(title: mock.title) // set view title
        
        // set switch status and show old value if its exist
        if DataProvider.shared.isEnabled(mock: mock) {
            isEnabled = true
            headerView.setOn(true) // set switch on
            actionView.isHidden = false // show text filed
            textField.text = DataProvider.shared.value(forMock: mock) as? String ?? "" // show saved value
        }
        else {
            isEnabled = false
            headerView.setOn(false) // disable switch
            actionView.isHidden = true // hide text filed
        }
    }
    
    func getValue() -> Any? {
        
        if isEnabled {
            return textField.text
        }
        else {
            return nil
        }
    }
    
    private func toggleStatus() {
        
        if isEnabled {
            actionView.isHidden = false
        }
        else {
            actionView.isHidden = true
        }
    }
    
    private func addDoneButtonToTextfieldKeeb() {
        
        let toolBar =  UIToolbar(frame: CGRect(
            origin: .zero,
            size: .init(width: UIScreen.main.bounds.width, height: 35)))
        toolBar.barStyle = .default
        toolBar.sizeToFit()
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(
            title: "Done",
            style: .plain,
            target: self,
            action: #selector(onTextfieldKeebDoneTap))
        if #available(iOS 13.0, *) {
            doneButton.tintColor = .systemIndigo
        } else {
            doneButton.tintColor = .purple
        }
        toolBar.items = [space, doneButton]
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
                                 
    @objc private func onTextfieldKeebDoneTap() {
        
        textField.resignFirstResponder()
    }
}

extension MockValueView: MockHeaderViewDelegate {
    
    func didChangeStatus(_ enabled: Bool) {
        
        isEnabled = enabled
        toggleStatus()
    }
    
}

extension MockValueView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
