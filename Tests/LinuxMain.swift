import XCTest

import TestingManagerTests

var tests = [XCTestCaseEntry]()
tests += TestingManagerTests.allTests()
XCTMain(tests)
