import XCTest
@testable import TestingManager

final class TestingManagerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TestingManager().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
